from csv import DictReader
import pandas as pd
from io import StringIO

class DataSet():
    def __init__(self, name = "train", path = "/content/drive/MyDrive/fnc-1", write = None, drop = None):
        self.path = path
        self.write = write
        self.drop = drop

        print("Reading dataset")
        bodies = name + "_bodies.csv"
        stances = name + "_stances.csv"
        # Read the stance file (containing headlines, body ID, and stances)
        self.stances = self.read(stances, self.write, self.drop)
        # Read the body file (containing body ID and articles)
        articles = self.read(bodies)
        self.articles = dict()

        # make the body ID an integer value
        for s in self.stances:
            s['Body ID'] = int(s['Body ID'])

        # copy all bodies into a dictionary
        for article in articles:
            self.articles[int(article['Body ID'])] = article['articleBody']

        print("Total stances: " + str(len(self.stances)))
        print("Total bodies: " + str(len(self.articles)))



    def read(self, filename, write = None, drop = None):
        rows = []
        with open(self.path + "/" + filename, "r", encoding = 'utf-8') as table:
            r = DictReader(table)
            if (drop is not None) and ('stances' in filename):
                r_df = pd.DataFrame(r)
                for label in drop:
                    r_df = r_df[r_df['Stance'] != label]
                buff = StringIO()
                r_df.to_csv(buff)
                buff.seek(0)
                r = DictReader(buff)
            if (write is not None) and ('stances' in filename):
                r_df = pd.DataFrame(r)
                columns = ['stat' + str(i) for i in range(len(write[0]))]
                w_df = pd.DataFrame(write, columns = columns)
                df = pd.concat([r_df, w_df], axis = 1)
                buff = StringIO()
                df.to_csv(buff)
                buff.seek(0)
                r = DictReader(buff)
            for line in r:
                rows.append(line)
        return rows
