% This must be in the first 5 lines to tell arXiv to use pdfLaTeX, which is strongly recommended.
\pdfoutput = 1
% In particular, the hyperref package requires pdfLaTeX in order to break URLs across lines.

\documentclass[11pt]{article}

% Remove the "review" option to generate the final version.
\usepackage{acl}

% Standard package includes
\usepackage{times}
\usepackage{latexsym}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{subfloat}

% \usepackage{titlesec}
% \setcounter{secnumdepth}{4}
% \titleformat{\paragraph}
% {\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
% \titlespacing*{\paragraph}
% {0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

% For proper rendering and hyphenation of words containing Latin characters (including in bib files)
\usepackage[T1]{fontenc}
% For Vietnamese characters
% \usepackage[T5]{fontenc}
% See https://www.latex-project.org/help/documentation/encguide.pdf for other character sets

% This assumes your files are encoded as UTF8
\usepackage[utf8]{inputenc}

% This is not strictly necessary, and may be commented out,
% but it will improve the layout of the manuscript,
% and will typically save some space.
\usepackage{microtype}

% If the title and author information does not fit in the area allocated, uncomment the following
%
%\setlength\titlebox{<dim>}
%
% and set <dim> to something 5cm or larger.

\title{Stance Detection with Step-by-step Binary Classification}

% Author information can be set in various styles:
% For several authors from the same institution:
% \author{Author 1 \and ... \and Author n \\
%         Address line \\ ... \\ Address line}
% if the names do not fit well on one line use
%         Author 1 \\ {\bf Author 2} \\ ... \\ {\bf Author n} \\
% For authors from different institutions:
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \And  ... \And
%         Author n \\ Address line \\ ... \\ Address line}
% To start a seperate ``row'' of authors use \AND, as in
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \AND
%         Author 2 \\ Address line \\ ... \\ Address line \And
%         Author 3 \\ Address line \\ ... \\ Address line}

\author{Jingtao Cao \\
  Electrical and Computer Engineering \\
  Department of Engineering \\
  \texttt{j75cao@uwaterloo.ca} \\\And
  Xuanzhi Huang \\
  Electrical and Computer Engineering \\
  Department of Engineering \\
  \texttt{x363huan@uwaterloo.ca} \\}

\begin{document}
\maketitle
\pagestyle{plain}
\begin{abstract}
As COVID‐19 pandemic restrained people from many social activities, online sources emerged as an essential platform to share and learn what happened in the world. Everyday people are seeking valuable information from the exploding volume of news, among which some information serves as misleading and deceiving for a secondary gain. When the degree to which online information can be regulated is beyond human effort, data scientists seek solutions from the Machine Learning and NLP perspective. A common pipeline would start with stance detection, which identifies fake news by comparing what other organisations are saying about a topic. This paper focused on this stage, proposed a number of techniques to perform this task on the FNC-1 dataset and compared them statistically. Specifically, this paper explored two directions which are Deep Learning models and modified baseline models. Lastly, existing models suffered from the imbalance of distribution of classes, then a multi-step structure to alleviate this problem was proposed.
\end{abstract}

\section{Related Work}
\subsection{Data Description}
The FNC-1 training dataset consists of 49972 headline-body pairs, with the length of headlines up to 22 words and the length of bodies up to 4788 words. Each pair is labelled with one of the stances that tells the relationship between them: unrelated, agree, disagree, or discuss. The test dataset contains 25413 unlabelled pairs.

\subsection{Language Features}
To enforce a reasonable training time and to focus on decisive characteristics, a number of feature extraction approaches were implemented by the baseline, and some useful methods are found in online resources.

For each instance, the baseline extracted Jaccard similarity which measures the similarity between the headline and the body, a binary vector denoting occurrences of refuting words, a binary feature denoting polarity of the headline, and the counts of n-grams with respect to tokens and characters. Below is the formula of Jaccard similarity:\
\begin{equation}
\begin{aligned}
J(A, B) = \frac{|A \cap B|}{|A \cup B|} = \frac{|A \cap B|}{|A| + |B| - |A \cap B|}
\end{aligned}
\end{equation}

Besides the features extracted by the baseline, the metric of term frequency-inverse document frequency (TF-IDF) was introduced, which simply implies the importance of a word in a particular document. TF-IDF increases proportionally to the occurrences of a word in the document and is offset by the number of documents that contain the word, which adjust for terms that appear frequently in general ~\cite{rajaraman_ullman_2011}. There are variations of TF-IDF, and the one below is adopted:
\begin{equation}
\begin{aligned}
\text{tf}(f, d) = f_{t, d}
\end{aligned}
\end{equation}
\begin{equation}
\begin{aligned}
\text{idf}(t, D) = \textnormal{log}\frac{N}{|{d\in D, t\in d}| + 1}
\end{aligned}
\end{equation}
\begin{equation}
\begin{aligned}
\text{tfidf}(t, d, D) = \textnormal{tf}(t, d) \cdot \textnormal{idf}(t, D)
\end{aligned}
\end{equation}
where the term frequency is the raw count of a word in a document, and the inverse document frequency tells if a word is common in the corpus.

\subsection{Deep Learning Models}
This task naturally implied the potential feasibility of common NLP models. Among them, Long Short Term Memory (LSTM) architecture is a special kind of RNN introduced by \cite{10.1162/neco.1997.9.8.1735}, which enables filtering out short term and long term dependencies accordingly. Figure~\ref{Figure1} is an illustration of LSTM architecture.
\begin{figure}[tp]
\begin{center}
\includegraphics[width = \linewidth]{LSTM.jpg}
\end{center}
\caption{\label{Figure1} Architecture of LSTM}
\end{figure}

Bidirectional Encoder Representations from Transformers (BERT), proposed by Google in 2018, is a transformer-based language model with a variable number of encoder layers and self-attention heads, which outperforms all the predecessors and stands as a ubiquitous baseline for most NLP tasks. BERT was pretrained on language modelling and next sentence prediction to learn contextual embeddings for words \cite{DBLP:journals/corr/abs-1810-04805}. Pretrained BERT can then be finetuned for specific tasks. Figure~\ref{Figure2} shows the structure.
\begin{figure}[tp]
\begin{center}
\includegraphics[width = \linewidth]{BERT.jpg}
\end{center}
\caption{\label{Figure2} Architecture of BERT}
\end{figure}

The GPT architecture implements a transformer model, which uses attention in place of previous recurrence- and convolution-based architectures \cite{DBLP:journals/corr/VaswaniSPUJGKP17}. The Attention mechanism enables the model to focus on segments of texts that it predicts to be the most relevant \cite{https://doi.org/10.48550/arxiv.1409.0473}. GPT outperforms previous benchmarks for RNN/CNN/LSTM-based models. GPT-2 has 1.5 billion parameters and is trained on 8 million web pages with a simple objective to predict the next word given the context. Figure~\ref{Figure3} displays the structure of GPT-2.
\begin{figure}[tp]
\begin{center}
\includegraphics[width = \linewidth]{GPT-2.jpg}
\end{center}
\caption{\label{Figure3} Architecture of GPT-2}
\end{figure}

\section{Data Preprocessing}
\subsection{Data Cleaning}
The raw data contains many non-essential words and characters that unnecessarily lengthen sentences and introduce noises to models. Before any steps occur, this should be handled. Regex expressions were used to remove special characters. Then, after tokenizing and lowering the sentences, a custom defined stopwords set is applied to remove matched tokens. Lastly, a lemmatization function from the NLTK library was applied to bring all words to their original formats.

\subsection{Data Embedding}
Google’s word2vec model GoogleNews vectors negative300 provides a convenient way to convert the language data to a computable form. The model contains 300-dimensional vectors for 3 million words and phrases and is able to index the majority of tokens in our training dataset \cite{W2V}. As the last step, padding is applied on short sentences and truncation is applied on lengthy sentences so that all pairs have fixed lengths.

% \subsection{Score System}
% Figure~\ref{Figure4} and~\ref{Figure5} suggest a highly unbalanced distribution of classes. Since telling the relevance between headlines and bodies is relatively an easy task, 25\% of the score weight is earned by telling whether a pair is relevant or irrelevant. Deemed as relevant, the rest 75\% is allocated to successfully distinguishing the classes among agree, disagree and discuss.
% \begin{figure}[htb]
% \begin{center}
% \includegraphics[width = \linewidth]{Stance Distribution of Training Set.jpg}
% \end{center}
% \caption{\label{Figure4} Stance Distribution of Training Set}
% \end{figure}

% \begin{figure}[htb]
% \begin{center}
% \includegraphics[width = \linewidth]{Stance Distribution of Test Set.jpg}
% \end{center}
% \caption{\label{Figure5} Stance Distribution of Test Set}
% \end{figure}

\section{Approaches}
\subsection{Baseline Model}
The baseline model used a Gradient Boosting classifier with hand-crafted features including Jaccard similarity, polarity, refutation, and n-grams. Table~\ref{Table1} and~\ref{Table2} ~\cite{baseline} show its predictions on the validation and test sets. This model achieved an accuracy of 87.74\% and a weighted score of 79.53\% (3538.0 out of 4448.5) on the hold-out set, and an accuracy of 86.32\% and a weighted score of 75.20\% (8761.75 out of 11651.25) on the test set.

\begin{table}[htb]
\begin{tabular}{l|llll}
\hline
          & agree & disagree & discuss & unrelated \\ \hline
agree     & 173   & 10       & 1435    & 28        \\
disagree  & 39    & 7        & 413     & 238       \\
discuss   & 221   & 7        & 3556    & 680       \\
unrelated & 10    & 3        & 358     & 17978     \\ \hline
\end{tabular}
\caption{\label{Table1} Predictions of Baseline Model on Hold-out Set}
\end{table}

\begin{table}[htb]
\begin{tabular}{l|llll}
\hline
          & agree & disagree & discuss & unrelated \\ \hline
agree     & 118   & 3       & 556    & 85        \\
disagree  & 14    & 3        & 130     & 15       \\
discuss   & 58   & 5        & 1527    & 210       \\
unrelated & 5    & 1        & 98     & 6794     \\ \hline
\end{tabular}
\caption{\label{Table2} Predictions of Baseline Model on Test Set}
\end{table}

\subsection{Proposed Approaches}
\subsubsection{NLP Models with Concatenated Inputs}
For each pair, the headline and the body are naively concatenated. The combinations are then fed into the pre-trained networks (BERT and GPT-2) to produce stance predictions. These two NLP networks are experimented to classify stances. Figure~\ref{Figure4} shows the workflow.
\begin{figure}[htp]
\begin{center}
\includegraphics[width = \linewidth]{LSTM v1.jpg}
\end{center}
\caption{\label{Figure4} NLP Models with Concatenated Inputs}
\end{figure}

\subsubsection{Concatenated LSTMs}
The raw data first followed the preprocessing steps mentioned in 2.1 and 2.2. Then, for each pair, the headline and the body of fixed length are going through two LSTMs respectively. The encodings produced by the two LSTMs are then concatenated, followed by a fully connected layer to output the final class prediction. For the architecture, see Figure~\ref{Figure5}.
\begin{figure}[htp]
\begin{center}
\includegraphics[width = 6cm, height = 8cm]{LSTM v2.jpg}
\end{center}
\caption{\label{Figure5} Concatenated LSTMs}
\end{figure}

\subsubsection{Modified Baseline}
Instead of building completely new models from scratch, the baseline model is a good starting point to introduce more features and model complexity.
\paragraph{Modified Features}
The accuracies of previous models were particularly low on predicting agree and disagree data. Based on this observation, more similar vocabularies are added into the refuting words corpus and a new agreeing words corpus is built to provide more perspectives of the data.

The original overlap and n-gram features considered stopwords in the sentences. Their new versions ignore stopwords to prevent noise and focus on the core ideas of the headlines and bodies.

For each pair, the term frequency (TF) and TF-IDF are computed to act as new statistical features. They will be used in the multimodal network.
\paragraph{Multimodal Network}
Opposite to concatenating all features into one vector in baseline, this approach separately feeds each feature into its corresponding multilayer perceptrons and concatenates the outputs, followed by a number of fully connected layers to make the final predictions. Figure~\ref{Figure6} shows the architecture of the multimodal network. The term frequency and TF-IDF features can also be used here.
\begin{figure}[htp]
\begin{center}
\includegraphics[width = \linewidth]{Multimodal Network.jpg}
\end{center}
\caption{\label{Figure6} Multimodal Network}
\end{figure}

\paragraph{Step-by-step Binary Classification}
Inspired by the scoring system and the unbalanced distribution of the data, this approach classified stances at 3 separate steps. The first step classified the data into two subgroups: discuss/unrelated, agree/disagree. In previous experiments, agree/disagree was prone to be misclassified as discuss/unrelated. As previous models directly divided data into four classes, tuning them solely for this issue risks affecting the performance of classifying other classes. Training a model to separate discuss/unrelated from agree/disagree first can effectively alleviate this issue. In later steps, each branch can be tuned to boost the performance of classifying two classes, while not affecting previous predictions. Figure~\ref{Figure7} is the illustration of step-by-step binary classification.
\begin{figure}[tp]
\begin{center}
\includegraphics[width = \linewidth]{Step-by-step Binary Classification.jpg}
\end{center}
\caption{\label{Figure7} Step-by-step Binary Classification}
\end{figure}

\section{Experiments and Results}
\subsection{Data Splitting}
For Deep Learning models, the provided data was split into training (90\%, 44975 instances) and validation sets (10\%, 4497 instances). Since the original dataset is large, only 10\% of it can reflect how well the fitted model generalizes on new data.

For models modified from the baseline, the splitting approach of the baseline was followed. The data was split into two parts: 80\% for training and 20\% for the hold-out set. For the training data, 10-fold cross-validation was implemented, and the model with the highest accuracy on the validation fold will be adopted for predictions.

\subsection{Evaluation Approach}
Since this task is to resolve a 4-class classification problem, the experiments were implemented by minimizing cross-entropy. For Deep Learning models, they were evaluated by calculating their accuracies on the validation set, and the hyperparameters were tuned accordingly. For modified baseline models, they were tuned and selected based on the accuracies on the validation folds, although their performance on the hold-out set was only for reference. After all the experiments, models with accuracies close to or higher than the baseline were used to predict stances for the test set.

\subsection{Experiments on Deep Learning Models}
There are three types of Deep Learning models that were tried: LSTM, BERT, and GPT-2. Each word is represented by its embedding, and the input of each pair of headline and body to the networks is a concatenated vector of embeddings. For the LSTM model, it consists of three LSTM layers, whose number of units are 128, 64, and 32 respectively, and each is followed by a dropout layer with rate of 0.2. Adam algorithm was used for optimization. For BERT and GPT-2, their pretrained versions were finetuned (especially the output layer) for use to save time and space. The hyperparameters in all these models were determined by experiments. Since the accuracies of LSTM and BERT are prone to converge after five epochs while GPT-2's performance tends to converge after 10 epochs, Figure~\ref{Figure8} to Figure~\ref{Figure10} only display the accuracies accordingly. It shows that a normal Deep Learning model fails to outperform the baseline. A reason could be that the corpus used by Google Word2Vec model is not highly related to news, and the various lengths of bodies make it difficult to have a proper threshold for truncation or padding.  
\begin{figure}[htp]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of LSTM.jpg}
\end{center}
\caption{\label{Figure8} Accuracies of LSTM}
\end{figure}

\vspace{0cm}

\begin{figure}[htp]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of BERT.jpg}
\end{center}
\caption{\label{Figure9} Accuracies of BERT}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of GPT-2.jpg}
\end{center}
\caption{\label{Figure10} Accuracies of GPT-2}
\end{figure}

To build a more complex model, the embedding vectors of each pair of headline and body were not connected anymore. Instead, they were fed into two LSTM layers respectively, each with 128 units, and their outputs were concatenated, followed by a dense layer and the output layer. Figure~\ref{Figure11} illustrates the accuracies of the first 10 training epochs. This model has a higher accuracy than the baseline.
\begin{figure}[htp]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of Concatenated LSTMs.jpg}
\end{center}
\caption{\label{Figure11} Accuracies of Concatenated LSTMs}
\end{figure}

\subsection{Experiments on Modified Baseline}
For all the modified baseline models, we changed the Gradient Boosting Classifier to Neural Networks since they cost less time for training while having similar or even better performance.
\subsubsection{Modified Features}
Besides building models from scratch, a few modifications were applied to improve the baseline. The first type of changes are new features.

It is shown from Table~\ref{Table1} and~\ref{Table2} that most samples with labels agree and disagree were misclassified. The True Positive Rate of class agree on the test set is 10.51\%, and that of class disagree is 5.60\%. Therefore, two new features were added to resolve this issue. To predict agree instances better, a corpus of synonyms of "agree" was used to count the total number of occurrences of these words in each headline. Besides, when computing Jaccard similarity and counting n-grams, stopwords were ignored to focus on the main content. Figure~\ref{Figure12} and~\ref{Figure13} show the accuracies of models with two features respectively. Both new features can improve the performance.
\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of Modified Baseline with Synonyms of Agree.jpg}
\end{center}
\caption{\label{Figure12} Accuracies of Modified Baseline with Synonyms of "Agree"}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of Modified Baseline without Stopwords in Overlap Feature and N-grams.jpg}
\end{center}
\caption{\label{Figure13} Accuracies of Modified Baseline without Stopwords in Overlap Feature and N-grams}
\end{figure}

\subsubsection{Multimodal Network}
Another type of modifications are on the architectures of models. Instead of concatenating four features as inputs, a new implementation is to input features to four independent MLPs (multi-layer perceptrons) and concatenate them, followed by a batch normalization layer and the output layer. Each multi-layer perceptron contains four layers with 256, 128, 64, and 32 units and ReLU activation functions. Again, Adam algorithm was used for optimization. Figure~\ref{Figure14} displays how accuracies change with epochs. The multimodal structure improves the performance of baseline. 
\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of Multimodal Network on 4 Baseline Features.jpg}
\end{center}
\caption{\label{Figure14} Accuracies of Multimodal Network on Four Baseline Features}
\end{figure}

Some new features as inputs to the multimodal network were also experimented. Each pair of headline and body were concatenated as a document, and the term frequency as well as TF-IDF of each word were calculated. These two new features were fed into two MLPs. Then, two networks were concatenated and followed by a fully connected layer and the output layer. The accuracy gradually converged after 30 epochs. Figure~\ref{Figure15} illustrates that this approach also boosts the model's performance.

Both multimodal networks perform better. Each sub-model extracts key information from each feature, which will be concatenated later. In contrast,  previous methods directly combine all the features and result in extremely high dimensions.
\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of Multimodal Network on TF-IDF and Term Frequency.jpg}
\end{center}
\caption{\label{Figure15} Accuracies of Multimodal Network on TF-IDF and Term Frequency}
\end{figure}

\subsubsection{Step-by-step Binary Classification}
It is known that the baseline model performs badly on agree and disagree instances. They are mostly classified as discuss or unrelated. Some features had been experimented to alleviate the issue. A new model architecture was tried. The baseline model directly predicts four classes, which makes it difficult to be improved for agree and disagree samples without making the performance on other classes worse off. The solution here is to predict whether an instance is of class discuss/unrelated or agree/disagree, and then do binary classification for each branch. In this way, the model can be tuned for the issue, while in each branch the two classes are highly different, so that the model can work well. Since the number of agree and disagree samples is much lower than other two classes, the first step is prone to predict all the instances to be unrelated/discuss, the agree/disagree data would be oversampled to balance the distribution.

There are five experiments conducted for the step-by-step classification approach. In the first experiment, all the agree and disagree data were copied and added to the training set; in the second one, two extra copies of agree and disagree samples are used; in the rest experiments, 90\%, 80\% and 70\% of the agree/disagree instances were copied twice as extra data respectively. In the first four experiments, the model misclassifies many discuss samples due to the imbalance between discuss and unrelated labels, so 50\% of the discuss data was additionally used in the first branch for the last experiment. In all these implementations, a two-layer MLP with units 32 and 8 was used for the first step, and for each branch a two-layer MLP with 128 and 64 units was used. Each fully connected layer uses ReLU activation function, and is followed by a dropout layer with rate of 0.2. In all experiments the model was trained for five epochs as accuracies changed slightly. Here only display the accuracies in the second and last experiments (Figure~\ref{Figure16a} to~\ref{Figure17c}). The rest figures can be found in Appendix 1. The model from Experiment 5 outperforms other networks on the binary classification between discuss and unrelated data.
\begin{subfigures}
\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with Two Copies of AgreeDisagree Data for Classifying DiscussUnrelated and AgreeDisagree.jpg}
\end{center}
\caption{\label{Figure16a} Accuracies of MLP with Two Copies of Agree/Disagree Data for Classifying Discuss/Unrelated and Agree/Disagree (Experiment 2)}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Discuss and Unrelated Data (Experiment 2).jpg}
\end{center}
\caption{\label{Figure16b} Accuracies of MLP for Discuss and Unrelated Data (Experiment 2)}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Agree and Disagree Data (Experiment 2).jpg}
\end{center}
\caption{\label{Figure16c} Accuracies of MLP for Agree and Disagree Data (Experiment 2)}
\end{figure}
\end{subfigures}

\begin{subfigures}
\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with Two Copies of 70 of AgreeDisagree Data for Classifying DiscussUnrelated and AgreeDisagree.jpg}
\end{center}
\caption{\label{Figure17a} Accuracies of MLP with Two Copies of 70\% of Agree/Disagree Data for Classifying Discuss/Unrelated and Agree/Disagree (Experiment 5)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with One Copy of 50 of Discuss Data for Discuss and Unrelated data (Experiment 5).jpg}
\end{center}
\caption{\label{Figure17b} Accuracies of MLP with One Copy of 50\% of Discuss Data for Discuss and Unrelated Data (Experiment 5)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Agree and Disagree Data (Experiment 5).jpg}
\end{center}
\caption{\label{Figure17c} Accuracies of MLP for Agree and Disagree Data (Experiment 5)}
\end{figure}
\end{subfigures}

\section{Conclusions}
Table~\ref{Table3} shows the performance of proposed approaches on the test set, and Table~\ref{Table4} to~\ref{Table5} in Appendix 2 display some example results from each model. It is shown that the step-by-step binary classification model can deal with small group stances better.
\begin{table}[ht]
\centering
\small
\begin{tabular}{ll}
\hline
\textbf{Model} & \textbf{Weighted Score} \\ \hline
\multicolumn{1}{l|}{LSTM}         
& 4749.50        \\ \hline
\multicolumn{1}{l|}{Concatenated LSTMs}         
& 6689.25        \\ \hline
\multicolumn{1}{l|}{Baseline with "Agree" Synonyms} 
& 9234.00        \\ \hline
\multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Baseline without Stopwords in Overlap \\ and N-grams\end{tabular}} & 9243.25        \\ \hline
\multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Multimodal Network on Four\\ Baseline Features\end{tabular}} 
& 9255.00           \\ \hline
\multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Multimodal Network on\\ TF-IDF and Term Frequency\end{tabular}} & 9113.00           \\ \hline
\multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Step-by-step Binary Classification\\ with Two Extra Copies of\\ 70\% of Agree and Disagree\\ and Extra 50\% of Discuss Data\end{tabular}} 
& 9324.75        \\ \hline
\end{tabular}
\caption{\label{Table3} Weighted Scores on Test Set}
\end{table}

It is concluded that:
\begin{enumerate}
  \item Deep Learning models using word embeddings are not suitable for this task. Even if Concatenated LSTMs has high accuracies on the validation set, it fails to generalize for test data.
  \item More information from agree/disagree and discuss instances can improve the performance of baseline, no matter by extracting new features (maybe for multimodal networks) or oversampling.
  \item Step-by-step binary classification helps to tune models one step at a time without heavily influencing model performance. It has the best performance among all the models.
\end{enumerate}

\section{Future Work}
As some bodies consist of thousands of words, it is too time-consuming and memory-consuming to establish NLP models even with effective embedding methods. On the other hand, the performance of the baseline which used basic feature extraction methods surpasses that of some NLP models, with much less training time and memory. For future work, it is better to explore more meaningful features that show the relationships between headlines and bodies in new perspectives.

The majority of the data stances are unrelated and discuss. Models are easily prone to misclassify stances of minor classes. In the future, more data of minor classes could be added to the training dataset to balance the distribution.

The previous trained models, especially NLP models, have high validation accuracies but low test accuracies at the same time. Future exploration is needed to resolve this issue.

\section*{Acknowledgements}
We would like to acknowledge and thank professor Olga Vechtomova and course teaching assistant Gaurav Sahu. The lectures taught by professor Vechtomova and the tutorials about Pytorch and Transformer provided by Gaurav are structured well and helpful for the project.

% Entries for the entire Anthology, followed by custom entries
\bibliography{anthology, custom}
\bibliographystyle{acl_natbib}

\newpage
\appendix
\section*{Appendix 1}

\setcounter{figure}{0}

\begin{subfigures}
\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with One Copy of AgreeDisagree Data for Classifying DiscussUnrelated and AgreeDisagree.jpg}
\end{center}
\caption{\label{Figure21} Accuracies of MLP with One Copy of Agree/Disagree Data for Classifying Discuss/Unrelated and Agree/Disagree (Experiment 1)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Discuss and Unrelated Data (Experiment 1).jpg}
\end{center}
\caption{\label{Figure22} Accuracies of MLP for Discuss and Unrelated Data (Experiment 1)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Agree and Disagree Data (Experiment 1).jpg}
\end{center}
\caption{\label{Figure23} Accuracies of MLP for Agree and Disagree Data (Experiment 1)}
\end{figure}
\end{subfigures}

\begin{subfigures}
\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with Two Copies of 90 of AgreeDisagree Data for Classifying DiscussUnrelated and AgreeDisagree.jpg}
\end{center}
\caption{\label{Figure24} Accuracies of MLP with Two Copies of 90\% of Agree/Disagree Data for Classifying Discuss/Unrelated and Agree/Disagree (Experiment 3)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Discuss and Unrelated Data (Experiment 3).jpg}
\end{center}
\caption{\label{Figure25} Accuracies of MLP for Discuss and Unrelated Data (Experiment 3)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Agree and Disagree Data (Experiment 3).jpg}
\end{center}
\caption{\label{Figure26} Accuracies of MLP for Agree and Disagree Data (Experiment 3)}
\end{figure}
\end{subfigures}

\begin{subfigures}
\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP with Two Copies of 80 of AgreeDisagree Data for Classifying DiscussUnrelated and AgreeDisagree.jpg}
\end{center}
\caption{\label{Figure27} Accuracies of MLP with Two Copies of 90\% of Agree/Disagree Data for Classifying Discuss/Unrelated and Agree/Disagree (Experiment 4)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Discuss and Unrelated Data (Experiment 4).jpg}
\end{center}
\caption{\label{Figure28} Accuracies of MLP for Discuss and Unrelated Data (Experiment 4)}
\end{figure}

\begin{figure}[h!bt]
\begin{center}
\includegraphics[width = \linewidth]{Accuracies of MLP for Agree and Disagree Data (Experiment 4).jpg}
\end{center}
\caption{\label{Figure29} Accuracies of MLP for Agree and Disagree Data (Experiment 4)}
\end{figure}
\end{subfigures}

\clearpage

\section*{Appendix 2}

\begin{table}[htp]
\centering
\small
\begin{tabular}{l|lllllllll}
\hline
Headline                          & Body ID & True Stance & C-LSTM    & Base w/ Agree & Base w/ Stopword \\ \hline
EXCLUSIVE: Apple To Unveil...     & 1964    & Agree       & Discuss   & Discuss       & Discuss          \\
Disgusting! Joan Rivers Doc...    & 1636    & Disagree    & Discuss   & Discuss       & Discuss          \\
Found! Missing Afghan Soldiers... & 2312    & Discuss     & Unrelated & Discuss       & Discuss          \\
Ferguson riots: Pregnant woman... & 2008    & Unrelated   & Unrelated & Unrelated     & Unrelated        \\ \hline
\end{tabular}
\caption{\label{Table4} Example Results from Models (Group 1)}
\end{table}

\begin{table}[htp]
\centering
\small
\begin{tabular}{l|lllllllll}
\hline
Headline                    & Body ID & True Stance & Multimodal w/ Base & Multimodal w/ TF-IDF & Step-by-step \\ \hline
EXCLUSIVE: Apple To...      & 1964    & Agree       & Discuss            & Discuss              & Agree        \\
Disgusting! Joan Rivers...  & 1636    & Disagree    & Discuss            & Discuss              & Discuss      \\
Found! Missing Afghan...    & 2312    & Discuss     & Discuss            & Discuss              & Discuss      \\
Ferguson riots: Pregnant... & 2008    & Unrelated   & Unrelated          & Unrelated            & Unrelated    \\ \hline
\end{tabular}
\caption{\label{Table5} Example Results from Models (Group 2)}
\end{table}

\end{document}
