import random
import os
from collections import defaultdict

# We have four parts of data: the competition_test is the test set, the hold-out part is the hold-out set that we use to evaluate our model
# for the rest data, we do 10 fold cross-validation, so we can train the model 10 times, have 10 validation sets, and thus we have 10 accuracies of the model, and we use the highest one as the performance of the model
# We tune and choose the model according to that value and see its performance on the hold-out and test sets (its predictions for the test set are we will submit)

def generate_hold_out_split(dataset, training = 0.8, base_dir = "splits"):
    r = random.Random()
    r.seed(1)

    article_ids = list(dataset.articles.keys())  # get a list of article ids
    r.shuffle(article_ids)  # and shuffle that list

    # Generate the body IDs of training samples and hold-out samples
    training_ids = article_ids[:int(training * len(article_ids))]
    hold_out_ids = article_ids[int(training * len(article_ids)):]

    # Write the split body IDs out to files for future use
    with open(base_dir + "/" + "training_ids.txt", "w+") as f:
        f.write("\n".join([str(id) for id in training_ids]))

    with open(base_dir + "/" + "hold_out_ids.txt", "w+") as f:
        f.write("\n".join([str(id) for id in hold_out_ids]))


# Read the body IDs of training/hold-out set
def read_ids(file, base):
    ids = []
    with open(base + "/" + file, "r") as f:
        for line in f:
           ids.append(int(line))
        return ids


def kfold_split(dataset, training = 0.8, n_folds = 10, base_dir = "splits"):
    # Use 100 * training% of the data for training and the rest for hold-out
    # Do n_fold cross-validation on the training set
    if not (os.path.exists(base_dir + "/" + "training_ids.txt")
            and os.path.exists(base_dir + "/" + "hold_out_ids.txt")):
        generate_hold_out_split(dataset, training, base_dir)

    training_ids = read_ids("training_ids.txt", base_dir)
    hold_out_ids = read_ids("hold_out_ids.txt", base_dir)

    # Store the n folds in a list
    folds = []
    for k in range(n_folds):
        folds.append(training_ids[int(k * len(training_ids) / n_folds): int((k + 1) * len(training_ids)/n_folds)])

    # "folds" contains n lists of body ID, each containing the body ID of articles in the corresponding fold,
    # "hold_out_ids" is a list that contains the body ID of articles in the hold-out set 
    return folds, hold_out_ids


# Generate a list of hold-out stances (including headlines, body ID, and stances)
# and a default dictionary of n folds of training stances, and the IDs of folds are indicated by keys
# Each element in the dictionary contains the stances of the corresponding fold ID
def get_stances_for_folds(dataset, folds, hold_out):
    stances_folds = defaultdict(list)
    stances_hold_out = []
    for stance in dataset.stances:
        if stance['Body ID'] in hold_out:
            stances_hold_out.append(stance)
        else:
            fold_id = 0
            for fold in folds:
                if stance['Body ID'] in fold:
                    stances_folds[fold_id].append(stance)
                fold_id += 1

    return stances_folds, stances_hold_out
